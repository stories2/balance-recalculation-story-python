import unittest


class BalanceStub(unittest.TestCase):
    def __init__(self):
        super().__init__()
        self.updated_sum = 0

    def update_balance(self, sum):
        self.updated_sum += sum

    def verify_updated_sum(self, expected_sum):
        self.assertEqual(expected_sum, self.updated_sum)
